Welcome to uxpSystems

Technology used.
-Maven
-Java8
-SpringBoot
- Spring security basic authentication
-Data JPA Hibernate implementation 
-REST
-Tomcat
-H2  In memory data base
-Junit
-Mockito

Version contorlling tool - BitBucket 

Please follow below steps to execute this project 

1. Clone the UxpSystem project from https://bitbucket.org/suhasBhosale/uxpsystems into desired folder.
	Note** will be sharing Git URL to clone the project, if you face any issue while accecing above URL use shared URL. 
2. open CMD from downloaded/cloned project floder and run  'mvn clean install'  
	Note : Above commond will build the project with all unit test cases.
3. go to the targer folder and run the executable jar using cmd 'java -jar uxpsystems-0.0.1-SNAPSHOT'.
4. Open the Rest client to perform the API testing, you can use the POSTMAN tool for the same.  	

Executes the below API to create, update, delete, getAll and Get user.
	For Basic Authentication need to provide Authentication userName: admin and Password: admin

	A. Create user End point  : http://localhost:8080/uxpsystem/user/
		Create user request :  {"name" : "UxpSystemTestUser1","password":"Pass12345", "status":"ACTIVE"}
	B. Update user End point  : http://localhost:8080/uxpsystem/user/{userId}
		Create user request :  {"name" : "UxpSystemTestUser1","password":"Pass123456", "status":"ACTIVE"}	
	C. GetAll users End point  : http://localhost:8080/uxpsystem/user/
	D. Get user End point  : http://localhost:8080/uxpsystem/user/{userId}
	E. Delete user End point  : http://localhost:8080/uxpsystem/user/{userId}

 Success http code 
 1. Create user : 201
 2. Update and Delete user : 202
 3 GetAll, Get : 200 (** Get/GetAll will return user json)
	
	
Below are the error code, reason and HTTP codes for faling response.

	1.  Http Code: 400 ,Error Code :1001, Error Message: Manadatory element ''{0}'' missing. 
	2.  Http Code: 400 ,Error Code :1002, User status is invalid, requested status value is ''{0}''. Status value shall be ACTIVE or DEACTIVE.
	3.  Http Code: 404 ,Error Code :1003, User does not exist for requested user id ''{0}''.
	4.  Http Code: 409 ,Error Code :1004, Error Message: User already exist for user name ''{0}''. User name must be unique throught the system. 
	5.  Http Code: 400 ,Error Code :1005, Unauthorised opration, user is not permitted to perform opration. Please contact system admin
	6.  Http Code: 404 ,Error Code :1006, No users found in the UXP systems 
	7.  Http Code: 500 ,Error Code :1010, Something went wrong while processing the request, the reason is ''{0}'. If this error persist please contact the Administrator.". 
	
	
If need any help please contact Suhas Bhosale.