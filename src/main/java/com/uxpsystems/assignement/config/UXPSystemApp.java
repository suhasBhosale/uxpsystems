package com.uxpsystems.assignement.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * The Class UXPSystemApp.
 */
@SpringBootApplication(scanBasePackages= {"com.uxpsystems"})
@EntityScan(basePackages = {"com.uxpsystems.assignement" })
@EnableJpaRepositories("com.uxpsystems.assignement")
public class UXPSystemApp{
	private final static Logger LOGGER = LogManager.getLogger(UXPSystemApp.class.getName());

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		
		SpringApplication.run(UXPSystemApp.class, args);
		
		LOGGER.info("UXP system application has been started successfully.");
	}

	

	
}
