package com.uxpsystems.assignement.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.uxpsystems.assignement.exception.ErrorCode;
import com.uxpsystems.assignement.exception.UxpServiceException;
import com.uxpsystems.assignement.exception.UxpSystemException;
import com.uxpsystems.assignement.util.ErrorType;

/**
 * The Class ExceptionHandlingController.
 */
@ControllerAdvice
public class ExceptionHandlingController {

	private final static Logger LOGGER = LogManager.getLogger(ExceptionHandlingController.class.getName());

	/**
	 * Handle UM service exception.
	 *
	 * @param serviceException
	 *            the service exception
	 * @return the response entity
	 */
	@ExceptionHandler(UxpServiceException.class)
	public ResponseEntity<ErrorType> handleUxpServiceException(final UxpServiceException serviceException) {
		LOGGER.info("Preparing the Response entity from  Uxp Service Exception. Error code : {}, Error message:  {}",
				serviceException.getErrorCode(), serviceException.getErrorMessage());
		final ErrorType errorType = new ErrorType(serviceException.getErrorMessage());
		return new ResponseEntity<ErrorType>(errorType, serviceException.getHttpStatus());
	}

	@ExceptionHandler(UxpSystemException.class)
	public ResponseEntity<ErrorType> handleUxpSystemException(final UxpSystemException systemException) {

		LOGGER.info("Preparing the Response entity from UxpSystemException. Error code : {}, Error message: {}",
				systemException.getErrorCode(), systemException.getErrorMessage());
		final ErrorType errorType = new ErrorType(systemException.getErrorMessage());
		return new ResponseEntity<ErrorType>(errorType, systemException.getHttpStatus());
	}

	/**
	 * Handle runtime exception.
	 *
	 * @param runtimeException
	 *            the runtime exception
	 * @return the response entity
	 */
	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<ErrorType> handlRuntimeException(final RuntimeException runtimeException) {
		LOGGER.info("Preparing the Response entity of RuntimeException.Error code : {}, Error message: {}",
				runtimeException.getCause(), runtimeException.getMessage());
		final UxpServiceException serviceException = ErrorCode.UXP_INTERNAL_SERVER_ERROR
				.createUxpServiceException(runtimeException, runtimeException.getMessage());
		final ErrorType errorType = new ErrorType(serviceException.getErrorMessage());

		return new ResponseEntity<ErrorType>(errorType, serviceException.getHttpStatus());
	}

}