package com.uxpsystems.assignement.controller;

import static com.uxpsystems.assignement.util.EndpointConstants.USER_MANAGEMENT;
import static com.uxpsystems.assignement.util.EndpointConstants.USER_MANAGEMENT_BY_ID;

import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.uxpsystems.assignement.domain.UserDomain;
import com.uxpsystems.assignement.exception.UxpServiceException;
import com.uxpsystems.assignement.exception.UxpSystemException;
import com.uxpsystems.assignement.service.IUserService;

/**
 * This class contains the API /Endpoints.
 */
@RestController
@RequestMapping("/")
public class UxpSystemsController {
	
	private final static Logger LOGGER = LogManager.getLogger(UxpSystemsController.class.getName());

	/** The user service. */
	@Autowired
	private IUserService userService;

	/**
	 * Find all users.
	 *
	 * @return the response entity
	 * @throws UxpServiceException 
	 */
	@GetMapping(value = USER_MANAGEMENT)
	public ResponseEntity<List<UserDomain>> findAllUsers() throws UxpServiceException {
		LOGGER.info("Started processing get all user request");
		final Set<UserDomain> users = userService.findAllUsers();
		if (users.isEmpty()) {
			LOGGER.warn("There are no users in the UXP systems");
			return new ResponseEntity<List<UserDomain>>(HttpStatus.NO_CONTENT);
		}
		LOGGER.info("Prepared the Users response entity and sending back to client");
		return new ResponseEntity(users, HttpStatus.OK);
	}

	/**
	 * Find user.
	 *
	 * @param id the id
	 * @return the response entity
	 * @throws UxpSystemException the user M service exception
	 */
	@RequestMapping(value = USER_MANAGEMENT_BY_ID, method = RequestMethod.GET)
	public ResponseEntity<?> findUser(@PathVariable("id") long id) throws UxpServiceException {
		LOGGER.info("Started processing to get  user with id {}"+id);
		final UserDomain user = userService.findById(id);
		return new ResponseEntity<UserDomain>(user, HttpStatus.OK);
	}
	
	/**
	 * Creates the user.
	 *
	 * @param user the user
	 * @param ucBuilder the uc builder
	 * @return the response entity
	 * @throws UxpSystemException the user M service exception
	 */
	@RequestMapping(value = USER_MANAGEMENT, method = RequestMethod.POST)
	public ResponseEntity<?> createUser(@RequestBody UserDomain user, UriComponentsBuilder ucBuilder) throws UxpServiceException {
		LOGGER.info("Started processing to Create user");
		userService.saveUser(user);
		return new ResponseEntity<HttpStatus>(HttpStatus.CREATED);
	}

	/**
	 * Update user.
	 *
	 * @param id the id
	 * @param user the user
	 * @return the response entity
	 * @throws UxpSystemException the user M service exception
	 */
	@RequestMapping(value = USER_MANAGEMENT_BY_ID, method = RequestMethod.PUT)
	public ResponseEntity<?> updateUser(@PathVariable("id") long id, @RequestBody UserDomain user) throws UxpServiceException {
		LOGGER.info("Started processing to update user with id {}"+id);
		user.setId(id);
		userService.updateUser(user);
		return new ResponseEntity<HttpStatus>(HttpStatus.ACCEPTED);
	}

	/**
	 * Delete user.
	 *
	 * @param id the id
	 * @return the response entity
	 * @throws UxpSystemException the user M service exception
	 */
	@RequestMapping(value = USER_MANAGEMENT_BY_ID, method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteUser(@PathVariable("id") long id) throws UxpServiceException {
		LOGGER.info("Started processing to delete user with id {}"+id);
		userService.deleteUserById(id);
		return new ResponseEntity<UserDomain>(HttpStatus.ACCEPTED);
	}

	/**
	 * Delete all users.
	 *
	 * @return the response entity
	 */
	/*@RequestMapping(value = USER_MANAGEMENT, method = RequestMethod.DELETE)
	public ResponseEntity<User> deleteAllUsers() {

		userService.deleteAllUsers();
		return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
	}*/

}