package com.uxpsystems.assignement.dao;

import java.util.Set;

import com.uxpsystems.assignement.domain.UserDomain;
import com.uxpsystems.assignement.exception.UxpServiceException;

/**
 * The Interface UserPersistence.
 */
public interface IUserDAO {
	
	/**
	 * Save user.
	 *
	 * @param user the user
	 * @return the user
	 * @throws UxpSystemException the user M service exception
	 */
	public UserDomain saveUser(UserDomain user) throws UxpServiceException;
	
	/**
	 * Gets the user.
	 *
	 * @param id the id
	 * @return the user
	 * @throws UxpSystemException the user M service exception
	 */
	public UserDomain getUser(long id) throws UxpServiceException;
	
	/**
	 * Gets the all user.
	 *
	 * @return the all user
	 * @throws UxpServiceException 
	 */
	public Set<UserDomain> getAllUser() throws UxpServiceException;
	
	/**
	 * Update user.
	 *
	 * @param user the user
	 * @return the user
	 * @throws UxpSystemException the user M service exception
	 */
	public UserDomain updateUser(UserDomain user) throws UxpServiceException;
	
	/**
	 * Delete user.
	 *
	 * @param id the id
	 * @return the user
	 * @throws UxpSystemException the user M service exception
	 */
	public void deleteUser(long id) throws UxpServiceException;
	
	/**
	 * Find user by name.
	 *
	 * @param userName the user name
	 * @return the user
	 */
	public UserDomain findUserByName(String  userName);

}
