package com.uxpsystems.assignement.dao;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.uxpsystems.assignement.domain.UserDomain;
import com.uxpsystems.assignement.entity.UserEntity;
import com.uxpsystems.assignement.exception.ErrorCode;
import com.uxpsystems.assignement.exception.UxpServiceException;
import com.uxpsystems.assignement.exception.UxpSystemException;
import com.uxpsystems.assignement.repositories.UserRepository;

/**
 * The Class UserPersistenceImpl.
 */
@Component
public class UserDAOImpl implements IUserDAO {
	/** The user repository. */
	private final static Logger LOGGER = LogManager.getLogger(UserDAOImpl.class.getName());

	
	@Autowired
	UserRepository userRepository;

	/**
	 * Instantiates a new user persistence impl.
	 *
	 * @param userRepository
	 *            the user repository
	 */

	/**
	 * {@inheritDoc}
	 * 
	 * @throws UxpSystemException
	 */
	@Override
	public UserDomain saveUser(final UserDomain user) throws UxpServiceException {
		return persistUser(user);
	}

	private UserDomain persistUser(final UserDomain user) throws UxpServiceException {

		final com.uxpsystems.assignement.entity.UserEntity userEntity = convertDomainToEntity(user);
		try {
			LOGGER.info("Started persisting user");
			final com.uxpsystems.assignement.entity.UserEntity savedUserEntity = userRepository.save(userEntity);
			return convertEntityToDomain(savedUserEntity);
		} catch (final Exception dbException) {
			if (dbException.getCause().toString().contains("org.hibernate.exception.ConstraintViolationException")) {
				LOGGER.error("Exception Occured Exception Occured while persisting user, the reason :"+dbException.getMessage());
				throw ErrorCode.USER_EXIST_FOR_NAME.createUxpServiceException(user.getName());
			} else {
				LOGGER.error("Exception Occured while persisting user. The reason :\"+dbException.getMessage()");
				
				throw ErrorCode.UXP_INTERNAL_SERVER_ERROR.createUxpServiceException(dbException);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @return
	 */
	@Override
	public UserDomain findUserByName(String userName) {
		LOGGER.info("Started Finding the user by user name "+ userName);
		return convertEntityToDomain(userRepository.findByName(userName));
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws UxpServiceException
	 */
	@Override
	public UserDomain getUser(final long id) throws UxpServiceException {
		final UserEntity userEntity = userRepository.getOne(id);
		try {
			LOGGER.info("Started getting user with id ",id);
			// Call the method, if proxy is not initialize it will give the Entity not find
			long userId = userEntity.getId();
			// log the error message
		} catch (final EntityNotFoundException entityNotFoundException) {
			LOGGER.error("Exception Occured");
			throw ErrorCode.USER_NOT_EXIST.createUxpServiceException(entityNotFoundException,
					Long.valueOf(id).toString());
		}

		return convertEntityToDomain(userEntity);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws UxpServiceException
	 */
	@Override
	public Set<UserDomain> getAllUser() throws UxpServiceException {
		LOGGER.info("Started getting all users");
		final List<com.uxpsystems.assignement.entity.UserEntity> savedUserEntities = userRepository.findAll();
		try {
			if (savedUserEntities.isEmpty()) {
				throw ErrorCode.NO_USERS.createUxpServiceException();
			}
		} catch (final RuntimeException proxyInitializeError) {
			LOGGER.error("Exception Occured");
			throw ErrorCode.NO_USERS.createUxpServiceException();
		}
		return convertEntityToDomain(savedUserEntities);

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws UxpServiceException
	 */
	// @Transactional( Isolation. )
	@Override
	public UserDomain updateUser(final UserDomain userDomain) throws UxpServiceException {
		LOGGER.info("Started updating user with id ",userDomain.getId());
		//To validate the user existence
		getUser(userDomain.getId());
		
		return persistUser(userDomain);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws UxpServiceException
	 */
	@Override

	public void deleteUser(final long id) throws UxpServiceException {
		LOGGER.info("Started deleting user with id ",id);

		// To validate is user exist for user id..
		getUser(id);
		userRepository.delete(id);
	}

	/**
	 * Convert entity to domain.
	 *
	 * @param savedUserEntity
	 *            the saved user entity
	 * @return the user
	 */
	private UserDomain convertEntityToDomain(com.uxpsystems.assignement.entity.UserEntity savedUserEntity) {
		LOGGER.info("Converting the user entity to domain.");
		UserDomain user = null;
		if (null != savedUserEntity) {
			user = new UserDomain();

			user.setId(savedUserEntity.getId());
			user.setName(savedUserEntity.getName());
			user.setPassword(savedUserEntity.getPassword());
			user.setStatus(savedUserEntity.getStats());
		}
		LOGGER.info("Converted the user entity to domain.");

		return user;
	}

	/**
	 * Convert domain to entity.
	 *
	 * @param userDomain
	 *            the user domain
	 * @return the com.javabycode.springboot.entity. user
	 */
	private com.uxpsystems.assignement.entity.UserEntity convertDomainToEntity(final UserDomain userDomain) {

		LOGGER.info("Converting the user domain entity.");
		final com.uxpsystems.assignement.entity.UserEntity userEntity = new com.uxpsystems.assignement.entity.UserEntity();
		userEntity.setId(userDomain.getId());
		userEntity.setName(userDomain.getName());
		// TODO encode pass before store the DB. we should not store the plain pass ..
		// Simple Base64.encode();
		userEntity.setPassword(userDomain.getPassword());
		userEntity.setStats(userDomain.getStatus());
		LOGGER.info("Converted the user domain entity.");

		return userEntity;
	}

	/**
	 * Convert entity to domain.
	 *
	 * @param savedUserEntities
	 *            the saved user entities
	 * @return the sets the
	 */
	private Set<UserDomain> convertEntityToDomain(
			List<com.uxpsystems.assignement.entity.UserEntity> savedUserEntities) {

		return savedUserEntities.parallelStream().map(persistedUser -> {
			final UserDomain domainUser = new UserDomain();

			domainUser.setId(persistedUser.getId());
			domainUser.setName(persistedUser.getName());
			domainUser.setPassword(persistedUser.getPassword());
			domainUser.setStatus(persistedUser.getStats());

			return domainUser;
		}).collect(Collectors.toSet());
	}

}
