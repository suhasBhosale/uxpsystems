package com.uxpsystems.assignement.exception;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

import java.text.MessageFormat;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.http.HttpStatus;

/**
 * The Enum ErrorCode.
 */
public enum ErrorCode {

	// Data validation error codes
	/** The manadatory element missing. */
	MANADATORY_ELEMENT_MISSING(1, "Manadatory element ''{0}'' missing.", BAD_REQUEST),

	/** The invalid user status. */
	INVALID_USER_STATUS(2,
			"User status is invalid, requested status value is ''{0}''. Status value shall be ACTIVE or DEACTIVE",
			BAD_REQUEST),

	// Persistence related error code
	/** The user not exist. */
	USER_NOT_EXIST(3, "User does not exist for requested user id ''{0}''.", NOT_FOUND),
	/** The user exist for name. */
	USER_EXIST_FOR_NAME(4, "User already exist for user name ''{0}''. User name must be unique throught the system.",
			CONFLICT),
	
	NO_USERS(6, "No users found in the UXP systems", NOT_FOUND), 
	
	// Service request authorisation error.
	/** The unauthorised opration. */
	UNAUTHORISED_OPRATION(5,
			"Unauthorised opration, user is not permitted to perform opration. Please contact system admin",
			UNAUTHORIZED),

	/** The um internal server error. */
	UXP_INTERNAL_SERVER_ERROR(10,
			"Something went wrong while processing the request, the reason is ''{0}'. If this error persist please contact the Administrator.",
			INTERNAL_SERVER_ERROR), 

	;

	/** The base erro code. */
	private int baseErroCode = 1000;

	/** The error code. */
	private int errorCode;

	/** The error message. */
	private String errorMessage;

	/** The http status. */
	private HttpStatus httpStatus;

	/**
	 * Instantiates a new error code.
	 *
	 * @param errorCode
	 *            the error code
	 * @param errorMessage
	 *            the error message
	 * @param httpStatus
	 *            the http status
	 */
	ErrorCode(final int errorCode, final String errorMessage, final HttpStatus httpStatus) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.httpStatus = httpStatus;
	}

	/**
	 * Creates the user management service exception.
	 *
	 * @param cause
	 *            the cause
	 * @param errorMessageParams
	 *            the error message params
	 * @return the user M service exception
	 */
	public UxpServiceException createUxpServiceException(final Throwable cause, final String... errorMessageParams) {

		final UxpServiceException serviceException = new UxpServiceException(cause);

		serviceException.setErrorCode(baseErroCode + errorCode);

		String formattedErroMesssage = MessageFormat.format(errorMessage, errorMessageParams);

		serviceException.setErrorMessage(formattedErroMesssage);
		serviceException.setHttpStatus(httpStatus);

		return serviceException;

	}

	/**
	 * Creates the uxp system exception.
	 *
	 * @param cause
	 *            the cause
	 * @param errorMessageParams
	 *            the error message params
	 * @return the uxp system exception
	 */
	public UxpSystemException createUxpSystemException(final Throwable cause, final String... errorMessageParams) {

		final UxpSystemException systemException = new UxpSystemException(cause);

		systemException.setErrorCode(baseErroCode + errorCode);

		String formattedErroMesssage = MessageFormat.format(errorMessage, errorMessageParams);

		systemException.setErrorMessage(formattedErroMesssage);
		systemException.setHttpStatus(httpStatus);

		return systemException;

	}

	/**
	 * Creates the user management service exception.
	 *
	 * @param errorMessageParams
	 *            the error message params
	 * @return the user M service exception
	 */
	public UxpServiceException createUxpServiceException(final String... errorMessageParams) {

		final UxpServiceException serviceException = new UxpServiceException();

		serviceException.setErrorCode(baseErroCode + errorCode);

		String formattedErroMesssage = MessageFormat.format(errorMessage, errorMessageParams);

		serviceException.setErrorMessage(formattedErroMesssage);
		serviceException.setHttpStatus(httpStatus);

		return serviceException;

	}

	/**
	 * Creates the uxp system exception.
	 *
	 * @param errorMessageParams
	 *            the error message params
	 * @return the uxp system exception
	 */
	public UxpSystemException createUxpSystemException(final String... errorMessageParams) {

		final UxpSystemException systemException = new UxpSystemException();

		systemException.setErrorCode(baseErroCode + errorCode);

		String formattedErroMesssage = MessageFormat.format(errorMessage, errorMessageParams);

		systemException.setErrorMessage(formattedErroMesssage);
		systemException.setHttpStatus(httpStatus);

		return systemException;

	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {

		// TODO remove this, this is for test the methods

		String plainClientCredentials = "upxsystemsadmin:upxsystemsadmin";
		String base64ClientCredentials = new String(Base64.encodeBase64(plainClientCredentials.getBytes()));
		System.out.println(base64ClientCredentials);

	}

}
