package com.uxpsystems.assignement.exception;

import org.springframework.http.HttpStatus;

/**
 * The Class UserMServiceException.
 */


public class UxpServiceException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The error message. */
	private String errorMessage;
	
	/** The http status. */
	private HttpStatus httpStatus;
	
	/** The error code. */
	private int errorCode;

	/**
	 * Instantiates a new user M service exception.
	 *
	 * @param cause the cause
	 */
	public UxpServiceException(Throwable cause) {
		super(cause.getMessage(), cause);
	}

	/**
	 * Instantiates a new user M service exception.
	 */
	public UxpServiceException() {
		super();
	}

	/**
	 * Gets the error code.
	 *
	 * @return the error code
	 */
	public int getErrorCode() {
		return errorCode;
	}

	/**
	 * Sets the error code.
	 *
	 * @param errorCode the new error code
	 */
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * Gets the error message.
	 *
	 * @return the error message
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * Sets the error message.
	 *
	 * @param errorMessage the new error message
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * Gets the http status.
	 *
	 * @return the http status
	 */
	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	/**
	 * Sets the http status.
	 *
	 * @param httpStatus the new http status
	 */
	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}

}
