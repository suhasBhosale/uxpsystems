package com.uxpsystems.assignement.service;


import java.util.Set;

import com.uxpsystems.assignement.domain.UserDomain;
import com.uxpsystems.assignement.exception.UxpServiceException;

/**
 * The Interface IUserService.
 */
public interface IUserService {
	
	/**
	 * Find by id.
	 *
	 * @param id the id
	 * @return the user
	 * @throws UxpSystemException the user M service exception
	 */
	UserDomain findById(long id) throws UxpServiceException;
	
	/**
	 * Find by name.
	 *
	 * @param name the name
	 * @return the user
	 */
	UserDomain findByName(String name);
	
	/**
	 * Save user.
	 *
	 * @param user the user
	 * @return the user
	 * @throws UxpSystemException the user M service exception
	 */
	UserDomain saveUser(UserDomain user) throws UxpServiceException;
	
	/**
	 * Update user.
	 *
	 * @param user the user
	 * @throws UxpSystemException the user M service exception
	 */
	void updateUser(UserDomain user) throws UxpServiceException;
	
	/**
	 * Delete user by id.
	 *
	 * @param id the id
	 * @throws UxpSystemException the user M service exception
	 */
	void deleteUserById(long id) throws UxpServiceException;

	/**
	 * Find all users.
	 *
	 * @return the sets the
	 * @throws UxpServiceException 
	 */
	Set<UserDomain> findAllUsers() throws UxpServiceException;
	
	
}
