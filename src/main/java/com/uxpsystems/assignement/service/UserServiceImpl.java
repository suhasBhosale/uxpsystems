package com.uxpsystems.assignement.service;

import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uxpsystems.assignement.dao.IUserDAO;
import com.uxpsystems.assignement.domain.UserDomain;
import com.uxpsystems.assignement.exception.UxpServiceException;
import com.uxpsystems.assignement.exception.UxpSystemException;
import com.uxpsystems.assignement.validation.IValidator;

/**
 * The Class UserServiceImpl.
 */
@Service("userService")
public class UserServiceImpl implements IUserService {
	private final static Logger LOGGER = LogManager.getLogger(UserServiceImpl.class.getName());

	/** The user validator. */
	private IValidator<UserDomain> userValidator;
	
	/** The user persistence. */
	private IUserDAO userPersistence;

	/**
	 * Instantiates a new user service impl.
	 *
	 * @param userValidator the user validator
	 * @param userPersistence the user persistence
	 */
	@Autowired
	private UserServiceImpl(final IValidator<UserDomain> userValidator, final IUserDAO userPersistence) {
		this.userValidator = userValidator;
		this.userPersistence = userPersistence;

	}

	/**
	 * {@inheritDoc}
	 * @return 
	 */
	@Override
	public UserDomain saveUser(final UserDomain user) throws UxpServiceException {
		LOGGER.info("Service invoked to save user ");
		userValidator.validate(user);
		final UserDomain savedUser = userPersistence.saveUser(user);
		//TODO replace by logger
		System.out.println("Saved user ID "+savedUser.getId());
		return savedUser;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws UxpSystemException
	 */
	@Override
	public void updateUser(final UserDomain user) throws UxpServiceException {
		LOGGER.info("Service invoked to update user ");
		userValidator.validate(user);
		userPersistence.updateUser(user);
	}

	/**
	 * {@inheritDoc}
	 * @throws UxpServiceException 
	 */
	@Override
	public Set<UserDomain> findAllUsers() throws UxpServiceException {
		LOGGER.info(" starting processing find all user service");
		return userPersistence.getAllUser();
	}

	/**
	 * {@inheritDoc}
	 * @throws UxpSystemException 
	 */
	@Override
	public UserDomain findById(long id) throws UxpServiceException {
		LOGGER.info(" starting processing find user service by user id "+ id);
		return userPersistence.getUser(id);
	}

	/**
	 * {@inheritDoc}
	 * @throws UxpSystemException 
	 */
	@Override
	public void deleteUserById(long id) throws UxpServiceException {
		LOGGER.info(" starting processing delete user service by user id "+ id);

		// TODO check is user exist
		userPersistence.deleteUser(id);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UserDomain findByName(String name) {

		LOGGER.info(" starting processing find user service by user name "+ name);
		throw new UnsupportedOperationException("Not yet implemented");
	}
}
