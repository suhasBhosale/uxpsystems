package com.uxpsystems.assignement.spring.security;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;

import com.uxpsystems.assignement.exception.ErrorCode;
import com.uxpsystems.assignement.exception.UxpServiceException;

/**
 * The Class UPXSystemBasicAuthEntryPoint.
 */
public class UPXSystemBasicAuthEntryPoint extends BasicAuthenticationEntryPoint {

	private final static Logger LOGGER = LogManager.getLogger(UPXSystemBasicAuthEntryPoint.class.getName());

	/* (non-Javadoc)
	 * @see org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint#commence(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, org.springframework.security.core.AuthenticationException)
	 */
	@Override
	public void commence(final HttpServletRequest request, final HttpServletResponse response,
			final AuthenticationException authException) throws IOException {
		
		LOGGER.info("Started validating the basic user authentication");
		// Authentication failed, send error response.
		final UxpServiceException serviceException = ErrorCode.UNAUTHORISED_OPRATION.createUxpServiceException();
		response.setStatus(serviceException.getHttpStatus().value());
		response.addHeader("WWW-Authenticate", "Basic realm=" + getRealmName() + "");
		response.sendError(serviceException.getErrorCode(), serviceException.getErrorMessage());
		PrintWriter writer = response.getWriter();
		writer.println(serviceException.getErrorCode() + "" + serviceException.getErrorMessage());
	}

	/* (non-Javadoc)
	 * @see org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint#afterPropertiesSet()
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		setRealmName(UPXSystemsConstatnts.SYSTEM_RELAM_NAME);
		super.afterPropertiesSet();
	}
}