package com.uxpsystems.assignement.spring.security;

/**
 * The Class UPXSystemsConstatnts for Basic security.
 */
public class UPXSystemsConstatnts {
	
	/** The system relam name. */
	// TODO configure the relam name in application.properties
	public static String SYSTEM_RELAM_NAME= "UXPSystemsRelam"; 
	
	/** The admin role. */
	public static String ADMIN_ROLE= "adminRole"; 
	
	/** The user role. */
	public static String USER_ROLE= "userRole"; 
	
	/** The admin user name. */
	public static String ADMIN_USER_NAME= "admin"; 
	
	/** The admin user pass. */
	public static String ADMIN_USER_PASS= "admin"; 
	
	/** The normal user name. */
	public static String NORMAL_USER_NAME= "user"; 
	
	/** The normal user pass. */
	public static String NORMAL_USER_PASS= "user"; 
	
}
