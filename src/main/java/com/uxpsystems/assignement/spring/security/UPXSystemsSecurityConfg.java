/*
 * 
 */
package com.uxpsystems.assignement.spring.security;

import static com.uxpsystems.assignement.spring.security.UPXSystemsConstatnts.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

/**
 * The Class UPXSystemsSecurityConfg.
 */
@Configuration
@EnableWebSecurity
public class UPXSystemsSecurityConfg extends WebSecurityConfigurerAdapter {
	private final static Logger LOGGER = LogManager.getLogger(UPXSystemsSecurityConfg.class.getName());

	/**
	 * Configure global security.
	 *
	 * @param auth the auth
	 * @throws Exception the exception
	 */
	@Autowired
	public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication().withUser(ADMIN_USER_NAME).password(ADMIN_USER_PASS).roles(ADMIN_ROLE);
		auth.inMemoryAuthentication().withUser(NORMAL_USER_NAME).password(NORMAL_USER_PASS).roles(USER_ROLE);
	}

	/* (non-Javadoc)
	 * @see org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter#configure(org.springframework.security.config.annotation.web.builders.HttpSecurity)
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.csrf().disable().authorizeRequests().antMatchers("/user/**").hasRole(ADMIN_ROLE).and().httpBasic()
				.realmName(UPXSystemsConstatnts.SYSTEM_RELAM_NAME).authenticationEntryPoint(getBasicAuthEntryPoint())
				.and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

		http.csrf().disable().authorizeRequests().antMatchers("/user/").hasRole(USER_ROLE).and().httpBasic()
		.realmName(UPXSystemsConstatnts.SYSTEM_RELAM_NAME).authenticationEntryPoint(getBasicAuthEntryPoint())
		.and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}

	/**
	 * Gets the basic auth entry point.
	 *
	 * @return the basic auth entry point
	 */
	@Bean
	public UPXSystemBasicAuthEntryPoint getBasicAuthEntryPoint() {
		return new UPXSystemBasicAuthEntryPoint();
	}

	/* (non-Javadoc)
	 * @see org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter#configure(org.springframework.security.config.annotation.web.builders.WebSecurity)
	 */
	/* To allow Pre-flight [OPTIONS] request from browser */
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**");
	}
}