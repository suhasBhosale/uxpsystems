package com.uxpsystems.assignement.util;

/**
 * The Class EndpointConstants.
 */
public class EndpointConstants {

	/** The Constant USER_MANAGEMENT. */
	public static final String USER_MANAGEMENT= "/user/";
	
	/** The Constant USER_MANAGEMENT_BY_ID. */
	public static final String USER_MANAGEMENT_BY_ID= "/user/{id}";
			
	
}
