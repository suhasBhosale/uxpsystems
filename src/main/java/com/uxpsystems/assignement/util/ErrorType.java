package com.uxpsystems.assignement.util;


/**
 * The Class ErrorType.
 */
public class ErrorType {

    /** The error message. */
    private String errorMessage;

    /**
     * Instantiates a new error type.
     *
     * @param errorMessage the error message
     */
    public ErrorType(String errorMessage){
        this.errorMessage = errorMessage;
    }

    /**
     * Gets the error message.
     *
     * @return the error message
     */
    public String getErrorMessage() {
        return errorMessage;
    }

}
