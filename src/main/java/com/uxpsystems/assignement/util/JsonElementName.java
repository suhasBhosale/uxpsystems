package com.uxpsystems.assignement.util;

/**
 * The Enum JsonElementName.
 */
public enum JsonElementName {
	
	/** The user name. */
	USER_NAME("user_name"), 
	
	/** The user add. */
	USER_PASSWORD("user_password"), 
	USER_STATUS("user_status"), 
	
	;

	/** The element name. */
	private String elementName;
	
	/**
	 * Instantiates a new json element name.
	 *
	 * @param elementName the element name
	 */
	private JsonElementName(final String elementName) {
		this.setElementName(elementName); 
	}
	
	/**
	 * Gets the element name.
	 *
	 * @return the element name
	 */
	public String getElementName() {
		return elementName;
	}
	
	/**
	 * Sets the element name.
	 *
	 * @param elementName the new element name
	 */
	public void setElementName(String elementName) {
		this.elementName = elementName;
	}
}
