package com.uxpsystems.assignement.util;

import org.springframework.util.StringUtils;

import com.uxpsystems.assignement.exception.ErrorCode;
import com.uxpsystems.assignement.exception.UxpServiceException;

/**
 * The Class ValidatorUtil.
 */
public class ValidatorUtil {

	/**
	 * Validate manadatory ele.
	 *
	 * @param elementvalue the elementvalue
	 * @param elementName the element name
	 * @throws UxpSystemException the user M service exception
	 * @throws UxpServiceException 
	 */
	public static void validateManadatoryEle(final String elementvalue, final String elementName)  throws UxpServiceException {

		if (StringUtils.isEmpty(elementvalue)) {

			throw ErrorCode.MANADATORY_ELEMENT_MISSING.createUxpServiceException(elementName);
		}
	}

}
