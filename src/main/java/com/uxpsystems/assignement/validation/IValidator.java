package com.uxpsystems.assignement.validation;

import com.uxpsystems.assignement.exception.UxpServiceException;

/**
 * The Interface IValidator.
 *
 * @param <T> the generic type
 */
public interface IValidator<T> {

	/**
	 * Validate.
	 *
	 * @param type the type
	 * @throws UxpSystemException the user M service exception
	 */
	public void validate(T type) throws UxpServiceException;
}
