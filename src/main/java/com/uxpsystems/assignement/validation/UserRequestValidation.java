package com.uxpsystems.assignement.validation;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import com.uxpsystems.assignement.common.StatusEnum;
import com.uxpsystems.assignement.domain.UserDomain;
import com.uxpsystems.assignement.exception.ErrorCode;
import com.uxpsystems.assignement.exception.UxpServiceException;
import com.uxpsystems.assignement.util.JsonElementName;
import com.uxpsystems.assignement.util.ValidatorUtil;

/**
 * The Class validates the add modify user data..
 */
// If there are multiple implementation of validator interface, need to
// @Component(value= "userRequestValidation")
// i.e. @autowire("userRequestValidation") where we require or inject specific
// implementation.
@Component
@Primary // This is primary/default implementation of validator.
public class UserRequestValidation implements IValidator<UserDomain> {

	// TODO Add regx for pass validation
	// final static String passRegPattern = ^[A-Z|a-z] {0}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.uxpsystems.assignement.validation.IValidator#validate(java.lang.Object)
	 */
	@Override
	public void validate(UserDomain user) throws UxpServiceException {
		ValidatorUtil.validateManadatoryEle(user.getName(), JsonElementName.USER_NAME.getElementName());
		ValidatorUtil.validateManadatoryEle(user.getPassword(), JsonElementName.USER_PASSWORD.getElementName());

		final String userRequestStatus = user.getStatus();
		final String statusElementName = JsonElementName.USER_STATUS.getElementName();
		ValidatorUtil.validateManadatoryEle(userRequestStatus, statusElementName);
		try {
			StatusEnum.valueOf(userRequestStatus);
		} catch (Exception exception) {
			throw ErrorCode.INVALID_USER_STATUS.createUxpServiceException(exception, userRequestStatus);
		}

	}

}
