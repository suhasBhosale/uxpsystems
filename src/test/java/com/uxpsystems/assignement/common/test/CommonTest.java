package com.uxpsystems.assignement.common.test;

import static com.uxpsystems.assignement.common.test.TestConstants.USER_ID;
import static com.uxpsystems.assignement.common.test.TestConstants.USER_NAME;
import static com.uxpsystems.assignement.common.test.TestConstants.USER_PASS;
import static com.uxpsystems.assignement.common.test.TestConstants.USER_STATUS;

import java.util.HashSet;
import java.util.Set;

import com.uxpsystems.assignement.domain.UserDomain;

/**
 * The Class CommonTest.
 */
public class CommonTest {
	/*
	 * @BeforeClass public void setUp() throws Exception {
	 * 
	 * //Nothing to do, in future will add generic setup for all test classes }
	 * 
	 * @AfterClass public void tearDown() { //Nothing to do, in future will add
	 * generic testDown for all test classes
	 * 
	 * }
	 */

	/**
	 * Prepare and get domain user.
	 *
	 * @return the user
	 */
	protected UserDomain prepareAndGetDomainUser() {
		final UserDomain user = new UserDomain();
		user.setId(USER_ID);
		user.setName(USER_NAME);
		user.setPassword(USER_PASS);
		user.setStatus(USER_STATUS);
		return user;
	}

	/**
	 * Prepare and get domain user.
	 *
	 * @return the user
	 */
	protected Set<UserDomain> prepareAndGetSetOfDomainUser(final int createUser) {
		Set<UserDomain> st = new HashSet<>();
		for (int userNo = 0; userNo < createUser; userNo++) {

			final UserDomain user = new UserDomain();
			user.setId(USER_ID);
			user.setName(USER_NAME);
			user.setPassword(USER_PASS);
			user.setStatus(USER_STATUS);
			st.add(user);
		}
		return st;
	}

	/**
	 * Prepare and get entity user.
	 *
	 * @return the com.uxpsystems.assignement.entity. user
	 */
	protected com.uxpsystems.assignement.entity.UserEntity prepareAndGetEntityUser() {
		final com.uxpsystems.assignement.entity.UserEntity user = new com.uxpsystems.assignement.entity.UserEntity();
		user.setId(USER_ID);
		user.setName(USER_NAME);
		user.setPassword(USER_PASS);
		user.setStats(USER_STATUS);
		return user;
	}
	
	
}
