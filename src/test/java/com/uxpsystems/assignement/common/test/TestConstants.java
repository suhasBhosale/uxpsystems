package com.uxpsystems.assignement.common.test;

import com.uxpsystems.assignement.common.StatusEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class TestConstants.
 */
public class TestConstants {
	
	/** The user id. */
	public static long USER_ID = 20; 
	/** The user name. */
	public static String USER_NAME = "testUser"; 
	public static String USER_PASS = "testUserPass"; 
	public static String USER_STATUS = StatusEnum.ACTIVE.name(); 
	
	
}
