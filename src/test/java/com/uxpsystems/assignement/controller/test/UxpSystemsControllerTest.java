package com.uxpsystems.assignement.controller.test;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.uxpsystems.assignement.common.test.CommonTest;
import com.uxpsystems.assignement.controller.UxpSystemsController;
import com.uxpsystems.assignement.domain.UserDomain;
import com.uxpsystems.assignement.service.UserServiceImpl;
import com.uxpsystems.assignement.util.EndpointConstants;

/**
 * The Class TestRestfulController.
 */
public class UxpSystemsControllerTest extends CommonTest {

	private MockMvc mockMvc;

	@Mock
	private UserServiceImpl userService;
	/*@Mock
	private Set<UserDomain> mocKUser;
*/
	@InjectMocks
	private UxpSystemsController uxpController;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(uxpController).build();
	}

	@Test
	public void findAllUsers() throws Exception {
		final Set<UserDomain> findUsers = prepareAndGetSetOfDomainUser(2);
		Mockito.when(userService.findAllUsers()).thenReturn(findUsers);		
		mockMvc.perform(get(EndpointConstants.USER_MANAGEMENT));
		
		verify(userService, times(1)).findAllUsers();
	    verifyNoMoreInteractions(userService);
	}

}
