package com.uxpsystems.assignement.dao.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import javax.validation.ConstraintViolationException;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;

import com.uxpsystems.assignement.common.test.CommonTest;
import com.uxpsystems.assignement.common.test.TestConstants;
import com.uxpsystems.assignement.dao.UserDAOImpl;
import com.uxpsystems.assignement.domain.UserDomain;
import com.uxpsystems.assignement.entity.UserEntity;
import com.uxpsystems.assignement.exception.ErrorCode;
import com.uxpsystems.assignement.exception.UxpServiceException;
import com.uxpsystems.assignement.repositories.UserRepository;

/**
 * The Class UserPersistenceTest.
 */
public class UserDAOImplTest extends CommonTest {

	/** The user repository mock. */
	@Mock
	private UserRepository userRepositoryMock;

	/** The user persistence impl. */
	@InjectMocks
	UserDAOImpl userPersistenceImpl;

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		// Mockito.reset(validatorMock, persistenceMock);
		MockitoAnnotations.initMocks(this);
		// Whitebox.setInternalState(userPersistenceImpl, "userRepository",
		// userRepository);
	}

	/**
	 * Test user already exist by name BC.
	 *
	 * @throws UxpServiceException
	 *             the user M service exception
	 */
	@Ignore
	@Test(expected = UxpServiceException.class)
	public void testUserAlreadyExistByName_BC() throws UxpServiceException {
		final String requestUserName = TestConstants.USER_NAME;
		final UserDomain requestUser = prepareAndGetDomainUser();
		// final com.uxpsystems.assignement.entity.UserEntity userEntity =
		// prepareAndGetEntityUser();
		final UxpServiceException actualException = ErrorCode.USER_EXIST_FOR_NAME
				.createUxpServiceException(requestUserName);
		final Exception exception = new Exception(new
		 ConstraintViolationException(null));
		// when(userRepositoryMock.findByName(anyString())).thenReturn(userEntity);
		doThrow(exception).when(userRepositoryMock).save(any(UserEntity.class));

		userPersistenceImpl.saveUser(requestUser);

	}

	/**
	 * Test save user GC.
	 *
	 * @throws UxpServiceException
	 *             the user M service exception
	 */
	@Test
	public void testSaveUser_GC() throws UxpServiceException {
		final UserDomain requestUser = prepareAndGetDomainUser();
		final com.uxpsystems.assignement.entity.UserEntity userEntity = prepareAndGetEntityUser();

		when(userRepositoryMock.findByName(anyString())).thenReturn(null);
		when(userRepositoryMock.save(userEntity)).thenReturn(userEntity);
		// Calling actual implementation for test the unit
		userPersistenceImpl.saveUser(requestUser);

	}

}
