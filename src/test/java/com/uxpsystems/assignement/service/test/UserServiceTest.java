package com.uxpsystems.assignement.service.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.uxpsystems.assignement.common.test.CommonTest;
import com.uxpsystems.assignement.common.test.TestConstants;
import com.uxpsystems.assignement.dao.IUserDAO;
import com.uxpsystems.assignement.domain.UserDomain;
import com.uxpsystems.assignement.exception.ErrorCode;
import com.uxpsystems.assignement.exception.UxpServiceException;
import com.uxpsystems.assignement.service.UserServiceImpl;
import com.uxpsystems.assignement.util.JsonElementName;
import com.uxpsystems.assignement.validation.IValidator;

/**
 * The Class UserServiceTest.
 */
public class UserServiceTest extends CommonTest {

	/** The validator mock. */
	@Mock
	private IValidator<UserDomain> validatorMock;
	
	/** The persistence mock. */
	@Mock
	private IUserDAO persistenceMock;
	
	/** The user service. */
	@InjectMocks
	UserServiceImpl userService;

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		// Mockito.reset(validatorMock, persistenceMock);
		MockitoAnnotations.initMocks(this);
		// Whitebox.setInternalState(userService, "userPersistence", persistenceMock);
	}

	/**
	 * Test save user GC.
	 *
	 * @throws UxpServiceException the user M service exception
	 */
	@Test
	public void testSaveUser_GC() throws UxpServiceException {
		final UserDomain savedUser = prepareAndGetDomainUser();
		when(persistenceMock.saveUser(any(UserDomain.class))).thenReturn(savedUser);
		userService.saveUser(savedUser);

	}


	/**
	 * Test save user GC.
	 *
	 * @throws UxpServiceException the user M service exception
	 */
	@Test
	public void testUpdateUser_GC() throws UxpServiceException {
		final UserDomain savedUser = prepareAndGetDomainUser();
		when(persistenceMock.updateUser(any(UserDomain.class))).thenReturn(savedUser);

		userService.updateUser(savedUser);

	}
	
	@Test(expected = UxpServiceException.class)
	public void testUpdateUser_BC() throws UxpServiceException {
		final UserDomain savedUser = prepareAndGetDomainUser();
		final UxpServiceException mandatoryEleMissing = ErrorCode.MANADATORY_ELEMENT_MISSING
				.createUxpServiceException(JsonElementName.USER_NAME.getElementName());
		doThrow(mandatoryEleMissing).when(validatorMock).validate(savedUser);
		userService.updateUser(savedUser);
		
	}

	
	/**
	 * Test save user GC.
	 *
	 * @throws UxpServiceException the user M service exception
	 */
	@Test
	public void testFindAllUserUser_GC() throws UxpServiceException {
		final Set<UserDomain> expectedAllUser = prepareAndGetSetOfDomainUser(2);
		when(persistenceMock.getAllUser()).thenReturn(expectedAllUser);
		final Set<UserDomain> actualGetAllUsers = userService.findAllUsers();
		
		assertEquals(expectedAllUser.size(), actualGetAllUsers.size());

	}
	
	/**
	 * Test validation failed BC.
	 *
	 * @throws UxpServiceException the user M service exception
	 */
	@Test(expected = UxpServiceException.class)
	public void testvalidationFailed_BC() throws UxpServiceException {
		final UserDomain savedUser = prepareAndGetDomainUser();
		final UxpServiceException expectedException = ErrorCode.USER_EXIST_FOR_NAME
				.createUxpServiceException(TestConstants.USER_NAME);
		when(persistenceMock.saveUser(any(UserDomain.class))).thenThrow(expectedException);
		try {
			userService.saveUser(savedUser);
		} catch (final UxpServiceException actualException) {

			assertEquals(expectedException.getMessage(), actualException.getMessage());
			assertEquals(expectedException.getHttpStatus(), actualException.getHttpStatus());
			assertEquals(expectedException.getErrorCode(), actualException.getErrorCode());

			throw actualException;

		}

	}

}
