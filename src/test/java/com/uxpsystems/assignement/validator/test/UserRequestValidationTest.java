package com.uxpsystems.assignement.validator.test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import com.uxpsystems.assignement.common.test.CommonTest;
import com.uxpsystems.assignement.domain.UserDomain;
import com.uxpsystems.assignement.exception.ErrorCode;
import com.uxpsystems.assignement.exception.UxpServiceException;
import com.uxpsystems.assignement.util.JsonElementName;
import com.uxpsystems.assignement.validation.UserRequestValidation;

/**
 * The Class UserServiceTest.
 */
public class UserRequestValidationTest extends CommonTest {

	/** The validator mock. */
	@InjectMocks
	private UserRequestValidation userRequestValidation;
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Test save user GC.
	 *
	 * @throws UxpServiceException the user M service exception
	 */
	@Test
	public void testUserValidator_GC() throws UxpServiceException {
		final UserDomain savedUser = prepareAndGetDomainUser();
		userRequestValidation.validate(savedUser);

	}
	
	@Test(expected = UxpServiceException.class)
	public void testUpdateUser_BC() throws UxpServiceException {
		UserDomain userReq = prepareAndGetDomainUser();
		userReq.setName(null);
		//Call the actual test
		commonTestMethod(userReq, JsonElementName.USER_NAME.getElementName());

		userReq = prepareAndGetDomainUser();
		userReq.setPassword(null);
		commonTestMethod(userReq, JsonElementName.USER_PASSWORD.getElementName());

		userReq = prepareAndGetDomainUser();
		userReq.setStatus(null);
		commonTestMethod(userReq, JsonElementName.USER_STATUS.getElementName());

		userReq.setStatus("InvalidStatus");
		commonTestMethod(userReq, JsonElementName.USER_STATUS.getElementName());
	}

	private void commonTestMethod(UserDomain savedUser, final String missingElementName) throws UxpServiceException {
		UxpServiceException expectedException = ErrorCode.MANADATORY_ELEMENT_MISSING
				.createUxpServiceException(missingElementName);
		try {
			userRequestValidation.validate(savedUser);
		}catch(UxpServiceException actualException) {
			assertEquals(expectedException.getMessage(), actualException.getMessage());
			assertEquals(expectedException.getHttpStatus(), actualException.getHttpStatus());
			assertEquals(expectedException.getErrorCode(), actualException.getErrorCode());

			throw actualException;
		}
	}

	
	
}
